# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pipewire
pkgver=0.3.55
pkgrel=0
pkgdesc="Multimedia processing graphs"
url="https://pipewire.org/"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	alsa-lib-dev
	avahi-dev
	bash
	bluez-dev
	dbus-dev
	doxygen
	eudev-dev
	fdk-aac-dev
	glib-dev
	graphviz
	gst-plugins-base-dev
	gstreamer-dev
	jack-dev
	libfreeaptx-dev
	libusb-dev
	libx11-dev
	meson
	ncurses-dev
	pulseaudio-dev
	py3-docutils
	readline-dev
	roc-toolkit-dev
	sbc-dev
	vulkan-loader-dev
	xmltoman
	"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-alsa
	$pkgname-pulse
	$pkgname-jack
	gst-plugin-pipewire:gst_plugin
	$pkgname-zeroconf
	$pkgname-spa-bluez
	$pkgname-spa-vulkan
	$pkgname-tools
	$pkgname-spa-tools:spa_tools
	$pkgname-libs
	$pkgname-lang
	"
install="$pkgname.post-upgrade"
source="https://gitlab.freedesktop.org/PipeWire/pipewire/-/archive/$pkgver/pipewire-$pkgver.tar.gz
	$pkgname-jack-fix.patch::https://gitlab.freedesktop.org/pipewire/pipewire/-/commit/40552a0e914c3aef48ce59ce1bfb9d80516aa893.patch
	pipewire.desktop
	pipewire-launcher.sh
	0001-Revert-pulse-tunnel-use-format-channels-and-rate-pro.patch
	"

case "$CARCH" in
	s390x)
		# libldac not available for big endian
		;;
	*)
		makedepends="$makedepends libldac-dev"
		;;
esac

case "$CARCH" in
	ppc64le|s390x|riscv64)
		# no webrtc-audio-processing
		;;
	*)
		makedepends="$makedepends webrtc-audio-processing-dev"
		subpackages="$subpackages $pkgname-echo-cancel:echo_cancel"
		;;
esac

build() {
	abuild-meson \
		-Dlibjack-path=/usr/lib \
		-Dlibv4l2-path=/usr/lib \
		-Ddocs=disabled \
		-Dman=enabled \
		-Dgstreamer=enabled \
		-Dexamples=enabled \
		-Dffmpeg=disabled \
		-Dsystemd=disabled \
		-Dvulkan=enabled \
		-Dsdl2=disabled \
		-Droc=enabled \
		-Dsession-managers=[] \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -Dm644 "$srcdir"/pipewire.desktop -t "$pkgdir"/etc/xdg/autostart/
	install -Dm755 "$srcdir"/pipewire-launcher.sh "$pkgdir"/usr/libexec/pipewire-launcher
}

dev() {
	default_dev

	mv "$subpkgdir"/usr/lib/libjack* "$pkgdir"/usr/lib/
}

alsa() {
	pkgdesc="ALSA support for pipewire"
	provides="pulseaudio-alsa=$pkgver-r$pkgrel"
	provider_priority=1
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/alsa-lib
	amove usr/share/alsa/alsa.conf.d

	mkdir -p "$subpkgdir"/etc/alsa/conf.d
	cp -r \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/*.conf \
		"$subpkgdir"/etc/alsa/conf.d/
}

pulse() {
	pkgdesc="Pulseaudio support for pipewire"
	depends="pipewire-session-manager"
	provides="pulseaudio=$pkgver-r$pkgrel pulseaudio-bluez=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/bin/pipewire-pulse
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-protocol-pulse.so
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-pulse-tunnel.so
	amove usr/share/pipewire/pipewire-pulse.conf

}

jack() {
	pkgdesc="JACK support for pipewire"
	depends="pipewire-session-manager"
	provides="jack=$pkgver-r$pkgrel"
	replaces="jack"

	amove usr/lib/libjack*
	amove usr/bin/pw-jack
	amove usr/lib/spa-*/jack/libspa-jack.so
	amove usr/share/pipewire/jack.conf
}

gst_plugin() {
	pkgdesc="Multimedia graph framework - PipeWire plugin"
	depends="pipewire-session-manager gst-plugins-base"

	amove usr/lib/gstreamer-1.0
}

echo_cancel() {
	pkgdesc="WebRTC-based echo canceller module for PipeWire"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-echo-cancel.so
}

zeroconf() {
	pkgdesc="$pkgdesc - Zeroconf support"
	depends=""
	provides="pulseaudio-zeroconf=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-zeroconf-discover.so
}

bluez() {
	pkgdesc="PipeWire BlueZ5 SPA plugin (Bluetooth)"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/spa-*/bluez5
}

vulkan() {
	pkgdesc="PipeWire Vulkan SPA plugin"
	depends=""

	amove usr/lib/spa-*/vulkan
}

tools() {
	pkgdesc="PipeWire tools"
	depends="$pkgname=$pkgver-r$pkgrel"
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/pw-*
}

spa_tools() {
	pkgdesc="PipeWire SPA tools"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/spa-*
}

sha512sums="
a139e51bc0d4144ae3bd3511962e541ae5c8a76d344d472805819e5bda6e62ec9807489c2a9e9f42bd9a25eff243f8a794b71d629438bf732a5cd92b85eaa9fc  pipewire-0.3.55.tar.gz
d7b9221e0da477cbc68f023ec390c5c2483b5ed6c84880ed3a99a7e787c3a94d278722ab10d88fa75827efe7cfd9903961780a92dbac88b33ddf68de6e261064  pipewire-jack-fix.patch
d5d8bc64e42715aa94296e3e26e740142bff7f638c7eb4fecc0301e46d55636d889bdc0c0399c1eb523271b20f7c48cc03f6ce3c072e0e8576c821ed1ea0e3dd  pipewire.desktop
be2bd1520fae27ccca6af4c98e8ebe63541260af55eb085839235a8441a7bce434ba8bf23a5d1ca8b5f361229f5482d5b63504b9a5cbbe9d39bc051207cd7dac  pipewire-launcher.sh
5a84a255794cd260476f93b154a32a84efc925c1f6ecc64efe659d89eb81bb3090438e2b3c4000a8ab68d8c72bca453e13297719a95f1e4457f43e43acec8bfa  0001-Revert-pulse-tunnel-use-format-channels-and-rate-pro.patch
"
